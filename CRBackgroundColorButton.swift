import UIKit

@IBDesignable
class ButtonBackgroundColor: UIButton {
	@IBInspectable var animationDurationOn: Double = 0.45
	@IBInspectable var animationDurationOff: Double = 0.3
    @IBInspectable var colorOffset: Double {
        didSet {
            generateHighlightColor()
        }
    }
    @IBInspectable var buttonColor: UIColor = UIColor.redColor() {
        didSet {
            backgroundColor = buttonColor
            generateHighlightColor()
        }
	}
	@IBInspectable var buttonHighlightColor: UIColor = UIColor.whiteColor()
	
    required init?(coder aDecoder: NSCoder) {
        colorOffset = -0.2
        buttonColor = UIColor.redColor()
        super.init(coder: aDecoder)
        backgroundColor = buttonColor
        generateHighlightColor()
    }
    
    override init(frame: CGRect) {
        colorOffset = -0.2
        buttonColor = UIColor.redColor()
        super.init(frame: frame)
        backgroundColor = buttonColor
        generateHighlightColor()
    }
    
	private func generateHighlightColor() {
        var hue: CGFloat = 0, sat: CGFloat = 0, val: CGFloat = 0, alp: CGFloat = 0
		buttonColor.getHue(&hue, saturation: &sat, brightness: &val, alpha: &alp)
		buttonHighlightColor = UIColor(hue: hue, saturation: sat, brightness: min(1, max(0, val + CGFloat(colorOffset))), alpha: alp)
	}
    
    private func highlightColorOn(aTouch: UITouch) {
        if self.buttonType == UIButtonType.System && aTouch.phase != .Began {
            if aTouch.phase != .Moved {
                layer.removeAllAnimations()
            }
            UIView.animateWithDuration(animationDurationOn, delay:0, options: [UIViewAnimationOptions.BeginFromCurrentState, UIViewAnimationOptions.AllowUserInteraction], animations: {
                self.backgroundColor = self.buttonHighlightColor
                }, completion: nil);
        }
        else {
            self.backgroundColor = self.buttonHighlightColor
        }
    }
    
    private func highlightColorOff(aTouch: UITouch) {
        if self.buttonType == UIButtonType.System {
            if aTouch.phase != .Moved && aTouch.phase != .Ended {
                layer.removeAllAnimations()
            }
            UIView.animateWithDuration(animationDurationOff, delay:0, options: [UIViewAnimationOptions.BeginFromCurrentState, UIViewAnimationOptions.AllowUserInteraction], animations: {
                self.backgroundColor = self.buttonColor
                }, completion: nil);
        }
        else {
            self.backgroundColor = self.buttonColor
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        highlightColorOn(touches.first!)
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesMoved(touches, withEvent: event)
        if let touch = touches.first
        {
            let touchPosition = touch.locationInView(self)
            if CGRectContainsPoint(CGRectInset(bounds, -70, -70), touchPosition) {
                highlightColorOn(touches.first!)
            }
            else {
                highlightColorOff(touches.first!)
            }
        }
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        super.touchesCancelled(touches, withEvent: event)
        if let touches = touches {
            highlightColorOff(touches.first!)
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        highlightColorOff(touches.first!)
    }
    
//    override func prepareForInterfaceBuilder() {
//        let fillLayer = CALayer()
//        fillLayer.bounds = bounds
//        fillLayer.position = CGPoint(x: frame.width/2, y: frame.height/2)
//        fillLayer.backgroundColor = buttonColor
//        layer.addSublayer(fillLayer)
//    }
}