# CR Components

Here is a bunch of personal components I create myself whenever I need them. All ClassNames are prefixed with **CR** to avoid name collision.  
Here is a list of them:

* CRObsidianViewController
    * A view controller that shares a dictionary with its children. When the dictionary is missing, it searches back into its parents.
* CRButtonBackgroundColor
    * A button with a background color changing when highlighted. You can use a color offset or set your own color.