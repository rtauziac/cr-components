//
//  CRObsidianViewController.h
//  
//
//  Created by Remy on 20/05/2015.
//
//

#import <UIKit/UIKit.h>

@interface CRObsidianViewController : UIViewController

@property (nonatomic, strong) NSMutableDictionary *obsidianUserInfo;
@property (nonatomic, readonly) BOOL ownsObsidianUserInfo;

@end
