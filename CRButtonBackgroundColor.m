//
//  CRButtonBackgroundColor.m
//  Yelloh Village
//
//  Created by Remy on 25/02/2015.
//  Copyright (c) 2015 Infostrates. All rights reserved.
//

#import "CRButtonBackgroundColor.h"

@implementation CRButtonBackgroundColor
-(void)generateHighlightColor
{
    CGFloat hue;
    CGFloat sat;
    CGFloat val;
    CGFloat alp;
    [self.buttonColor getHue:&hue saturation:&sat brightness:&val alpha:&alp];
    _buttonHighlightColor = [UIColor colorWithHue:hue saturation:sat brightness:MIN(1, MAX(0, val + self.colorOffset)) alpha:alp];
}

-(instancetype)init {
    self = [super init];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void)commonInit {
    _buttonColor = self.backgroundColor;
    
    self.colorAnimationDurationOn = 0.45;
    self.colorAnimationDurationOff = 0.3;
    self.colorOffset = -0.2;
    
    [self generateHighlightColor];
    
    [self addTarget:self action: @selector(highlightColorOn:event:) forControlEvents: UIControlEventTouchDown];
    [self addTarget:self action: @selector(highlightColorOn:event:) forControlEvents: UIControlEventTouchDragEnter];
    [self addTarget:self action: @selector(highlightColorOff:event:) forControlEvents: UIControlEventTouchUpInside];
    [self addTarget:self action: @selector(highlightColorOff:event:) forControlEvents: UIControlEventTouchCancel];
    [self addTarget:self action: @selector(highlightColorOff:event:) forControlEvents: UIControlEventTouchUpOutside];
    [self addTarget:self action: @selector(highlightColorOff:event:) forControlEvents: UIControlEventTouchDragExit];
}

-(void)setColorOffset:(CGFloat)colorOffset
{
    _colorOffset = colorOffset;
    [self generateHighlightColor];
}

-(void)setButtonColor:(UIColor *)buttonColor
{
    _buttonColor = buttonColor;
    self.backgroundColor = _buttonColor;
    [self generateHighlightColor];
}

-(void)highlightColorOn:(UIView*)sender event:(UIEvent*)event
{
    UITouch *aTouch = ((UITouch*)[[event allTouches] anyObject]);
    if (self.buttonType == UIButtonTypeSystem && aTouch.phase != UITouchPhaseBegan)
    {
        if (aTouch.phase != UITouchPhaseMoved) {
            [self.layer removeAllAnimations];
        }
        [UIView animateWithDuration:self.colorAnimationDurationOn delay:0 options: UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations: ^{
            self.backgroundColor = self.buttonHighlightColor;
        } completion: nil];
    }
    else {
        self.backgroundColor = self.buttonHighlightColor;
    }
}

-(void)highlightColorOff:(UIView*)sender event:(UIEvent*)event
{
    if (sender != (id)event) { // I had a bug where self, sender and event were the same object
        UITouch *aTouch = ((UITouch*)[[event allTouches] anyObject]);
        if (self.buttonType == UIButtonTypeSystem)
        {
            if (aTouch.phase != UITouchPhaseMoved) {
                [self.layer removeAllAnimations];
            }
//            [self.layer removeAllAnimations];
            [UIView animateWithDuration:self.colorAnimationDurationOff delay:0 options: UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction animations: ^{
                self.backgroundColor = self.buttonColor;
            } completion: nil];
        }
        else {
            self.backgroundColor = self.buttonColor;
        }
    }
}

-(void)setOverrideHighlightColor:(UIColor *)overrideColor
{
    _buttonHighlightColor = overrideColor;
}

@end
