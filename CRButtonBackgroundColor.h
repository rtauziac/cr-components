//
//  CRButtonBackgroundColor.h
//  Yelloh Village
//
//  Created by Remy on 25/02/2015.
//  Copyright (c) 2015 Infostrates. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CRButtonBackgroundColor : UIButton

@property (nonatomic, readwrite) NSTimeInterval colorAnimationDurationOn; // 0.45
@property (nonatomic, readwrite) NSTimeInterval colorAnimationDurationOff; // 0.3
@property (nonatomic, readwrite) IBInspectable CGFloat colorOffset; // -0.2 // didSet { generateHighlightColor() }
@property (nonatomic, readwrite) IBInspectable UIColor *buttonColor; // didSet { backgroundColor = buttonColor; generateHighlightColor() }
@property (nonatomic, readonly) UIColor *buttonHighlightColor;

-(instancetype)init;
-(instancetype)initWithCoder:(NSCoder *)aDecoder;
-(instancetype)initWithFrame:(CGRect)frame;

-(void)generateHighlightColor;
-(void)setOverrideHighlightColor:(UIColor*)overrideColor;

@end
