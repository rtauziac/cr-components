//
//  ISObsidianViewController.m
//  
//
//  Created by Remy on 20/05/2015.
//
//

#import "CRObsidianViewController.h"

@interface CRObsidianViewController ()

@end

@implementation CRObsidianViewController

-(NSMutableDictionary*)obsidianUserInfo
{
    if ([self.parentViewController respondsToSelector:@selector(obsidianUserInfo)]) {
        return self.obsidianUserInfo ?: ((CRObsidianViewController*)self.parentViewController).obsidianUserInfo;
    }
    else{
        if (_obsidianUserInfo == nil) {
            _obsidianUserInfo = [NSMutableDictionary dictionary];
        }
        return _obsidianUserInfo;
    }
}

-(BOOL)ownsObsidianUserInfo
{
    return _obsidianUserInfo != nil;
}

@end
